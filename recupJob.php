<<<<<<< HEAD
<?php

	//Définitions des tailles
	define('KB', 1024);
	define('MB', 1048576);
	define('GB', 1073741824);
	define('TB', 1099511627776);

	//Contenu du mail en utf-8 pour les accents / caractères spéciaux.
	header('Content-Type: text/html; charset=utf-8');

	//Récupération des données avec la variable globale POST.
	$pour = htmlspecialchars($_POST['pour']);
	$agence = htmlspecialchars($_POST['agence']);
	$niveau = htmlspecialchars($_POST['niveau']);
	$domaine = htmlspecialchars($_POST['domaine']);
	$date_debut = htmlspecialchars($_POST['date_debut']);
	$duree = htmlspecialchars($_POST['duree']);
	$nom = htmlspecialchars($_POST['nom']);
	$prenom = htmlspecialchars($_POST['prenom']);
	$mail = htmlspecialchars($_POST['mail']);
	$tel = htmlspecialchars($_POST['tel']);
	$msg = htmlspecialchars($_POST['msg']);

	//...Si l'utilisateur postule pour un emploi, alors on n'affiche pas la durée (et changement des entêtes : "début de l'emploi / stage")
	if($pour == "Emploi"){
		$postule = "Candidature d'emploi de la part de " . $nom . " " . $prenom;
		$message = "Nom : " . $nom ."<br>".
		"Prénom : ". $prenom ."<br>".
		"Postule dans l'agence de : " . $agence ."<br>".
		"Niveau d'études : " . $niveau ."<br>".
		"Domaine : " . $domaine ."<br>".
		"Date de début de l'emploi : ".$date_debut."<br>".
		"Adresse mail : ".$mail."<br>".
		"Téléphone : " . $tel ."<br>".
		"Message : " . $msg ."<br>";
	}
	//...Sinon (il postule pour un stage, on affiche la durée et les entêtes changent)
	else{
		$postule = "Candidature de stage de la part de " . $nom . " " . $prenom;
		$message = "Nom : " . $nom ."<br>".
		"Prénom : ". $prenom ."<br>".
		"Postule dans l'agence de : " . $agence ."<br>".
		"Niveau d'études : " . $niveau ."<br>".
		"Domaine : " . $domaine ."<br>".
		"Date de début du stage : ".$date_debut."<br>".
		"Durée du stage : ".$duree."<br>".
		"Adresse mail : ".$mail."<br>".
		"Téléphone : " . $tel ."<br>".
		"Message : " . $msg ."<br>";
	}

	//Appel de la méthode "PHPMailer" (non native a PHP, il faut la télécharger et l'installer en local / sur le serveur) pour la gestion de l'envoi de mails
	require('C:\xampp\composer\vendor\autoload.php');
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;
	
	//Instanciation d'un nouvel objet PHPMailer
    $mail = new PHPMailer;

    $mail->IsSMTP(); //On précise l'usage du SMTP (pour l'envoi du mail) [à noter qu'il faut aussi changer les paramètres dans php.ini]
    $mail->SMTPAuth = true; //On doit s'authentifier (si notre SMTP en a besoin)
    $mail->SMTPSecure = 'starttls'; //Protocule de sécurisation de notre SMTP (si il en a besoin)
   	$mail->CharSet = 'UTF-8'; //Le jeu de caractères pour les accents
   	$mail->Encoding = 'base64'; //Egalement nécéssaire pour l'affichage des caractères
    $mail->Host = "smtp.netcourrier.com"; //Notre SMTP (par ex. smtp.orange.fr, smtp.gmail.com...)
    $mail->Port = 587; //Le port de notre SMTP (ex : 25, 467 ou 587 en fonction du SMTP)
    $mail->IsHTML(true); //Notre contenu est du HTML, on doit le préciser.
    $mail->Username = "exemple@exemple.com"; //Login SMTP
    $mail->Password = "exemple"; //Mot de passe SMTP
    $mail->SetFrom("exemple@.com"); //L'adresse mail qui envoie
    $mail->Subject = $sujet; //Le sujet de notre mail
    $mail->Body = $message; //Le corps de notre mail
    $mail->AddAddress("exempleo@gmail.com"); //L'adresse mail du destinataire (devra être info@bulko.net)

	//Add the allowed mime-type files to an 'allowed' array 
	$allowed = array('doc', 'docx', 'txt', 'pdf');

	//Check uploaded file type is in the above array (therefore valid)  
	if(in_array(pathinfo($_FILES['uploaded_file']['name'], PATHINFO_EXTENSION), $allowed)){

		//If filetypes allowed types are found, continue to check filesize:
		if($_FILES["uploaded_file"]["size"] > 2*MB){
		    //Envoi de la pièce jointe...
		    if (isset($_FILES['uploaded_file']) &&
			    $_FILES['uploaded_file']['error'] == UPLOAD_ERR_OK) {
			    $mail->AddAttachment($_FILES['uploaded_file']['tmp_name'],
			                         $_FILES['uploaded_file']['name']);
			}
			if(!$mail->send()) {
			    echo 'Message could not be sent.';
			    echo 'Mailer Error: ' . $mail->ErrorInfo;
			} else {
			    echo 'Message has been sent';
			}
			//Confirmation de l'envoi a l'utilisateur.
		    //header("Location:index.php?sent=1");
		}
		else{
			echo "Erreur : fichier trop volumineux. (Ne doit pas dépasser 2Mo).";
		}
	}
	else{
		echo "Erreur : mauvais format de pièce jointe";
	}
?>
=======
<!DOCTYPE html>
<html>
<head>
	<meta charset = "utf-8"/>
	<title>Récupération job</title>
</head>
<body>
	<?php

		//Définitions des tailles
		define('KB', 1024);
		define('MB', 1048576);
		define('GB', 1073741824);
		define('TB', 1099511627776);

		//Contenu du mail en utf-8 pour les accents / caractères spéciaux.
		header('Content-Type: text/html; charset=utf-8');

		//Récupération des données avec la variable globale POST.
		$pour = htmlspecialchars($_POST['pour']);
		$agence = htmlspecialchars($_POST['agence']);
		$niveau = htmlspecialchars($_POST['niveau']);
		$domaine = htmlspecialchars($_POST['domaine']);
		$date_debut = htmlspecialchars($_POST['date_debut']);
		$duree = htmlspecialchars($_POST['duree']);
		$nom = htmlspecialchars($_POST['nom']);
		$prenom = htmlspecialchars($_POST['prenom']);
		$mail = htmlspecialchars($_POST['mail']);
		$tel = htmlspecialchars($_POST['tel']);
		$msg = htmlspecialchars($_POST['msg']);

		//...Si l'utilisateur postule pour un emploi, alors on n'affiche pas la durée (et changement des entêtes : "début de l'emploi / stage")
		if($pour == "Emploi"){
			$postule = "Candidature d'emploi de la part de " . $nom . " " . $prenom;
			$message = "Nom : " . $nom ."<br>".
			"Prénom : ". $prenom ."<br>".
			"Postule dans l'agence de : " . $agence ."<br>".
			"Niveau d'études : " . $niveau ."<br>".
			"Domaine : " . $domaine ."<br>".
			"Date de début de l'emploi : ".$date_debut."<br>".
			"Adresse mail : ".$mail."<br>".
			"Téléphone : " . $tel ."<br>".
			"Message : " . $msg ."<br>";
		}
		//...Sinon (il postule pour un stage, on affiche la durée et les entêtes changent)
		else{
			$postule = "Candidature de stage de la part de " . $nom . " " . $prenom;
			$message = "Nom : " . $nom ."<br>".
			"Prénom : ". $prenom ."<br>".
			"Postule dans l'agence de : " . $agence ."<br>".
			"Niveau d'études : " . $niveau ."<br>".
			"Domaine : " . $domaine ."<br>".
			"Date de début du stage : ".$date_debut."<br>".
			"Durée du stage : ".$duree."<br>".
			"Adresse mail : ".$mail."<br>".
			"Téléphone : " . $tel ."<br>".
			"Message : " . $msg ."<br>";
		}

		//Appel de la méthode "PHPMailer" (non native a PHP, il faut la télécharger et l'installer en local / sur le serveur) pour la gestion de l'envoi de mails
		require('C:\xampp\composer\vendor\autoload.php');
		use PHPMailer\PHPMailer\PHPMailer;
		use PHPMailer\PHPMailer\Exception;
		
		//Instanciation d'un nouvel objet PHPMailer
	    $mail = new PHPMailer;

	    $mail->IsSMTP(); //On précise l'usage du SMTP (pour l'envoi du mail) [à noter qu'il faut aussi changer les paramètres dans php.ini]
	    $mail->SMTPAuth = true; //On doit s'authentifier (si notre SMTP en a besoin)
	    $mail->SMTPSecure = 'starttls'; //Protocule de sécurisation de notre SMTP (si il en a besoin)
	   	$mail->CharSet = 'UTF-8'; //Le jeu de caractères pour les accents
	   	$mail->Encoding = 'base64'; //Egalement nécéssaire pour l'affichage des caractères
	    $mail->Host = "smtp.netcourrier.com"; //Notre SMTP (par ex. smtp.orange.fr, smtp.gmail.com...)
	    $mail->Port = 587; //Le port de notre SMTP (ex : 25, 467 ou 587 en fonction du SMTP)
	    $mail->IsHTML(true); //Notre contenu est du HTML, on doit le préciser.
	    $mail->Username = "pablocorrales@netcourrier.com"; //Login SMTP
	    $mail->Password = "DvE$29*28XKn"; //Mot de passe SMTP
	    $mail->SetFrom("pablocorrales@netcourrier.com"); //L'adresse mail qui envoie
	    $mail->Subject = $postule; //Le sujet de notre mail
	    $mail->Body = $message; //Le corps de notre mail
	    $mail->AddAddress("pablo.corrales.pro@gmail.com"); //L'adresse mail du destinataire

		//Add the allowed mime-type files to an 'allowed' array 
		$allowed = array('doc', 'docx', 'txt', 'pdf');

		//Check uploaded file type is in the above array (therefore valid)  
		if(in_array(pathinfo($_FILES['uploaded_file']['name'], PATHINFO_EXTENSION), $allowed)){

			//If filetypes allowed types are found, continue to check filesize:
			if($_FILES["uploaded_file"]["size"] < 2*MB){
			    //Envoi de la pièce jointe...
			    if (isset($_FILES['uploaded_file']) &&
				    $_FILES['uploaded_file']['error'] == UPLOAD_ERR_OK) {
				    $mail->AddAttachment($_FILES['uploaded_file']['tmp_name'],
				                         $_FILES['uploaded_file']['name']);
				}
				if(!$mail->send()) {
				    echo 'Message could not be sent.';
				    echo 'Mailer Error: ' . $mail->ErrorInfo;
				} else {
				    echo 'Message has been sent';
				}
				//Confirmation de l'envoi a l'utilisateur.
			    //header("Location:index.php?sent=1");
			}
			else{
				echo "Erreur : fichier trop volumineux. (Ne doit pas dépasser 2Mo).";
			}
		}
		else{
			echo "Erreur : mauvais format de pièce jointe";
		}
	?>
</body>
</html>
>>>>>>> master
