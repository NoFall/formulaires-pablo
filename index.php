<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>Formulaires</title>
	<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<!-- Confirmation de l'envoi de mail à l'utilisateur avec la variable globale $_GET-->
	<?php
		if(isset($_GET["sent"]) == 1){
	?>
	<div>Votre message a bien été envoyé.</div>
	<?php
		}
	?>

	<form action="recupJob.php" method="POST" enctype="multipart/form-data">
		<h2> Job / Stage </h2>
		
		<label>Postuler pour :</label>
		<br>
		<select id="pour" name="pour">
			<option value="Stage">Stage</option>
			<option value="Emploi">Emploi</option>
		</select>
		<br> <br>

		<label> Agence de :</label>
		<br>
		<select id="agence" name="agence">
			<option value="Bordeaux">Bordeaux</option>
			<option value="Genève">Genève</option>
		</select>
		<br> <br>

		<label> Niveau* :</label>
		<br>
		<select id="niveau" name="niveau" required="required">
			<option value>- - -</option>
			<option value="Bac">Bac</option>
			<option value="Bac +1">Bac +1</option>
			<option value="Bac +2">Bac +2</option>
			<option value="Bac +3">Bac +3</option>
			<option value="Bac +4">Bac +4</option>
			<option value="Bac +5">Bac +5</option>
		</select>
		<br><br>

		<label> Domaine* :</label>
		<br>
		<select id="domaine" name="domaine" required="required">
			<option value>- - -</option>
			<option value="Stratégie et suivi de projet">Stratégie et suivi de projet</option>
			<option value="Création et graphisme">Création et graphisme</option>
			<option value="Développement commercial">Développement commercial</option>
			<option value="Développement informatique">Déveleppement informatique</option>
			<option value="Community management">Community management</option>
			<option value="Autre">Autre</option>
		</select>
		<br><br>

		<label>Date de début* (JJ/MM/AAAA) :</label>
		<br>
			<input type = "text" name="date_debut" id="date_debut" required="required" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}">
		<br><br>

		<label>Durée (si stage) :</label>
		<br>
		<select id="duree" name="duree">
			<option value>- - -</option>
			<option value="1 mois">1 mois</option>
			<option value="2 mois">2 mois</option>
			<option value="3 mois">3 mois</option>
			<option value="4 mois">4 mois</option>
			<option value="5 mois ou +">5 mois ou +</option>
		</select>
		<br><br>

		<label>Nom* :</label>
		<br>
			<input type = "text" name="nom" id="nom" required="required">
		<br><br>

		<label>Prénom* :</label>
		<br>
			<input type = "text" name="prenom" id="prenom" required="required">
		<br><br>

		<label>Email* :</label>
		<br>
			<input type = "text" name="mail" id="mail" required="required" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$">
		<br><br>

		<label>Téléphone* :</label>
		<br>
			<input type = "text" name="tel" id="tel" required="required" pattern="0+[0-9]{9}">
		<br><br>

		<label>C.V.* : (fichier doc ou pdf, < 2Mo) </label>
		<br>
			<input type="file" name="uploaded_file" id="uploaded_file" required="required" accept=".pdf,.doc,.docx,.jpg,.jpeg,.png, application/msword, application/pdf, image/jpeg, image/png">
		<br><br>

		<label>Motivations* :</label>
		<br>
			<textarea name = "msg" id = "msg" rows = "6" cols = "50" required="required"></textarea>
		<br><br>
		<p>La société BULKO a mis en place ce formulaire de contact afin de pouvoir échanger sur ses thématiques commerciales professionnelles. Les informations recueillies sont enregistrées directement dans la messagerie du dirigeant. Elles sont utilisées exclusivement pour la réponse aux messages des visiteurs du site internet bulko.fr.<br>
		Les données à caractère personnel ne sont pas conservées hormis sur la durée de la relation commerciale et ne font l’objet d’aucun transfert ni d’aucune transmission.<br>
		Conformément au « Règlement Général sur la Protection des Données », vous disposez d’un droit d’accès, de rectification, et d’effacement des données vous concernant, ainsi qu’un droit à la limitation du traitement de ces données. Vous disposez également de la possibilité de retirer à tout moment votre consentement au traitement de vos données pour les finalités mentionnées ci-dessus. Vous pouvez exercer vos droits en contactant BULKO par email à l'adresse : info@bulko.net</p>
		<input type = "checkbox" name = "rgpd" value="ok" class="rgpd" required="required">
		<br><br>
		<input type="submit" name="submit" id = submit alt="Envoi" value = "OK"/>
		<br>
	</form>

	<form action="recupContact.php" method="POST">
		<p>----------------------------------------------------------</p>
		<h2>Contact</h2>

		<label>Nom* :</label>
		<br>
			<input type = "text" name="nom" id="nom" required="required">
		<br><br>

		<label>Prénom* :</label>
		<br>
			<input type = "text" name="prenom" id="prenom" required="required">
		<br><br>

		<label>Société :</label>
		<br>
			<input type = "text" name="societe" id="societe">
		<br><br>

		<label>Pays :</label>
		<br>
			<input type="text" name="pays" id="pays">
		<br><br>

		<label>Email* :</label>
		<br>
			<input type="text" name="mail" id="mail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$">
		<br><br>

		<label>Téléphone* :</label>
		<br>
			<input type="text" name="tel" id="tel" pattern="0+[0-9]{9}">
		<br><br>

		<label>Commentaire* :</label>
		<br>
			<textarea name="msg" id="msg" rows="6" cols="50" required="required"></textarea>
		<br><br>
		<p>La société BULKO a mis en place ce formulaire de contact afin de pouvoir échanger sur ses thématiques commerciales professionnelles. Les informations recueillies sont enregistrées directement dans la messagerie du dirigeant. Elles sont utilisées exclusivement pour la réponse aux messages des visiteurs du site internet bulko.fr.<br>
		Les données à caractère personnel ne sont pas conservées hormis sur la durée de la relation commerciale et ne font l’objet d’aucun transfert ni d’aucune transmission.<br>
		Conformément au « Règlement Général sur la Protection des Données », vous disposez d’un droit d’accès, de rectification, et d’effacement des données vous concernant, ainsi qu’un droit à la limitation du traitement de ces données. Vous disposez également de la possibilité de retirer à tout moment votre consentement au traitement de vos données pour les finalités mentionnées ci-dessus. Vous pouvez exercer vos droits en contactant BULKO par email à l'adresse : info@bulko.net</p>
		<input type = "checkbox" name = "rgpd" value="ok" class="rgpd" required="required">
		<br><br>
		<input type="submit" name="submit" id = submit alt="Envoi" value = "OK"/>
		<br>
</body>
</html>